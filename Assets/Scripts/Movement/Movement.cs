﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float Speed;

    void Update()
    {
        Vector3 Target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Target.z = transform.position.z;

        transform.position = Vector3.MoveTowards(transform.position, Target, Speed * Time.deltaTime / transform.localScale.x);
        this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, -15, 15), Mathf.Clamp(this.transform.position.y, -15, 15), this.transform.position.z);


    }
}
