﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMain : MonoBehaviour
{
    public Transform Target;
    public float Speed = 10;

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, Speed * Time.deltaTime);
        Vector3 cameraClamp = transform.position;
        cameraClamp.z = Mathf.Clamp(0, 0, -10);
        transform.position = cameraClamp;
    }
}
