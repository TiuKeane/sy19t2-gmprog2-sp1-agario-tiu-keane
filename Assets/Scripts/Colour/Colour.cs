﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;

public class Colour : MonoBehaviour
{
    public List<Material> Mat = new List<Material>();

    void Awake()
    {
        GetComponent<Renderer>().material = Mat[Random.Range(0, Mat.Count)];
    }
}
